<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EcommerceSubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        DB::table('ecommerce_sub_categories')->insert([
            [
                'subcategory' => 'Dummy 1',
                'subcategory_id' => 1,
            ],

            [
                'subcategory' => 'Dummy 2',
                'subcategory_id' => 2,
            ],
        ]);
    }
}
