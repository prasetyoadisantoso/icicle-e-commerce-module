<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EcommerceSkuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('ecommerce_skues')->insert([
            [
                'sku' => '001',
                'caption' => 'This is Caption',
                'regular_price' => 10000,
                'sale_price' => 1000,
                'image' => '',
                'description' => 'This is Description',
                'sku_id' => 1,
            ],

            [
                'sku' => '002',
                'caption' => 'This is Caption',
                'regular_price' => 10000,
                'sale_price' => 5000,
                'image' => '',
                'description' => 'This is Description',
                'sku_id' => 2,
            ],



        ]);

        // $this->call("OthersTableSeeder");
    }
}
