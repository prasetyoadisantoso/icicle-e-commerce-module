<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EcommerceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('ecommerce_categories')->insert([
            [
                'category' => 'dummy',
                'image' => 'dummy.jpg',
                'category_id' => 1,
            ],

            [
                'category' => 'dummy',
                'image' => 'dummy.jpg',
                'category_id' => 2,
            ],


        ]);

    }
}
