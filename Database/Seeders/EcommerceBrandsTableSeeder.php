<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EcommerceBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ecommerce_brands')->insert([
            [
                'brand' => 'dummy',
                'image' => 'dummy.jpg',
                'brand_id' => 1,
            ],

            [
                'brand' => 'dummy',
                'image' => 'dummy.jpg',
                'brand_id' => 2,
            ],


        ]);
    }
}
