<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EcommerceShippingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('ecommerce_shippings')->insert([
            [
                'address' => 'Dummies Street, No 1',
                'weight' => 20,
                'height' => 15,
                'width' => 1,
                'shipping_id' => 1,
            ],

            [
                'address' => 'Dummies Street, No 2',
                'weight' => 20,
                'height' => 15,
                'width' => 1,
                'shipping_id' => 2,
            ],



        ]);

        // $this->call("OthersTableSeeder");
    }
}
