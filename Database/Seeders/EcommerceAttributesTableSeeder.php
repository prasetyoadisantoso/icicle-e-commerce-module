<?php

namespace Modules\Ecommerce\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EcommerceAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('ecommerce_attributes')->insert([
            [
                'attribute' => 'dummy',
                'image' => 'dummy.jpg',
                'attribute_id' => 1,
            ],

            [
                'attribute' => 'dummy',
                'image' => 'dummy.jpg',
                'attribute_id' => 2,
            ],


        ]);

        // $this->call("OthersTableSeeder");
    }
}
