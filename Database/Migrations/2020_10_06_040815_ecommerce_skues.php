<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EcommerceSkues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ecommerce_skues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku');
            $table->string('caption');
            $table->decimal('regular_price');
            $table->decimal('sale_price');
            $table->string('image');
            $table->string('description');
            $table->bigInteger('sku_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
