<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceCategories extends Model
{
    protected $fillable = ['category', 'image'];

    protected $casts = [
        'image' => 'array',
    ];
}
