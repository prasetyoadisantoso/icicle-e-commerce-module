<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceShippings extends Model
{
    protected $fillable = ['address', 'weight', 'height', 'width', 'shipping_id'];
}
