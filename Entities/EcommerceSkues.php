<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceSkues extends Model
{
    protected $fillable = ['sku', 'caption', 'regular_price', 'sale_price',  'description'];

    protected $casts = [
        'image' => 'array'
    ];
}
