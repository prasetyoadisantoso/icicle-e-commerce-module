<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceProducts extends Model
{
    protected $fillable = ['product', 'caption', 'regular_price', 'sale_price', 'description'];

    protected $casts = [
        'image' => 'array',
    ];
}
