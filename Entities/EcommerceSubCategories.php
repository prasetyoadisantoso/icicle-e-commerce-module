<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceSubCategories extends Model
{
    protected $fillable = ['subcategory'];
}
