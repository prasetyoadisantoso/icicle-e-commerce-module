<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceAttributes extends Model
{
    protected $fillable = ['attribute', 'image'];

    protected $casts = [
        'image' => 'array',
    ];
}
