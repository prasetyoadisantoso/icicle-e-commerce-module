<?php

namespace Modules\Ecommerce\Entities;

use Illuminate\Database\Eloquent\Model;

class EcommerceBrands extends Model
{
    protected $fillable = ['brand', 'brand_id'];

    protected $casts = [
        'image' => 'array',
    ];
}
