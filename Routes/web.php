<?php

/*
|--------------------------------------------------------------------------
| Ecommerce Routes
|--------------------------------------------------------------------------
|
| Here is where you can register ecommerce routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Ecommerce */
Route::prefix('ecommerce')->group(function() {

    Route::get('/', 'EcommerceController@index')->name('ecommerce.index');

});




/* Products */
Route::prefix('ecommerce/products')->group(function() {

    Route::get('/', 'ProductsController@index')->name('products.index');
    Route::get('create', 'ProductsController@create')->name('products.create');
    Route::post('store', 'ProductsController@store')->name('products.store');
    Route::get('edit/{id}', 'ProductsController@edit')->name('products.edit');
    Route::put('update/{id}', 'ProductsController@update');
    Route::delete('delete/{id}', 'ProductsController@destroy');

});


/* Brands */
Route::prefix('ecommerce/brands')->group(function() {

    Route::get('/', 'BrandsController@index')->name('brands.index');
    Route::get('create', 'BrandsController@create')->name('brands.create');
    Route::post('store', 'BrandsController@store')->name('brands.store');
    Route::get('edit/{id}', 'BrandsController@edit')->name('brands.edit');
    Route::put('update/{id}', 'BrandsController@update');
    Route::delete('delete/{id}', 'BrandsController@destroy');


});

/* Categories */
Route::prefix('ecommerce/categories')->group(function() {


    Route::get('/', 'CategoriesController@index')->name('categories.index');
    Route::get('create', 'CategoriesController@create')->name('categories.create');
    Route::post('store', 'CategoriesController@store')->name('categories.store');
    Route::get('edit/{id}', 'CategoriesController@edit')->name('categories.edit');
    Route::put('update/{id}', 'CategoriesController@update');
    Route::delete('delete/{id}', 'CategoriesController@destroy');


});


/* Attributes*/
Route::prefix('ecommerce/attributes')->group(function() {
    Route::get('/', 'AttributesController@index')->name('attributes.index');
    Route::get('create', 'AttributesController@create')->name('attributes.create');
    Route::post('store', 'AttributesController@store')->name('attributes.store');
    Route::get('edit/{id}', 'AttributesController@edit')->name('attributes.edit');
    Route::put('update/{id}', 'AttributesController@update');
    Route::delete('delete/{id}', 'AttributesController@destroy');
});



/* Shippings */
Route::prefix('ecommerce/shippings')->group(function() {
    Route::get('/', 'ShippingsController@index')->name('shippings.index');
    Route::get('create', 'ShippingsController@create')->name('shippings.create');
    Route::post('store', 'ShippingsController@store')->name('shippings.store');
    Route::get('edit/{id}', 'ShippingsController@edit')->name('shippings.edit');
    Route::put('update/{id}', 'ShippingsController@update');
    Route::delete('delete/{id}', 'ShippingsController@destroy');
});

/* SKU */
Route::prefix('ecommerce/sku')->group(function() {
    Route::get('/', 'SkuController@index')->name('sku.index');
    Route::get('create', 'SkuController@create')->name('sku.create');
    Route::post('store', 'SkuController@store')->name('sku.store');
    Route::get('edit/{id}', 'SkuController@edit')->name('sku.edit');
    Route::put('update/{id}', 'SkuController@update');
    Route::delete('delete/{id}', 'SkuController@destroy');
});


/* SubCategories */
Route::prefix('ecommerce/subcategories')->group(function() {
    Route::get('/', 'SubCategoriesController@index')->name('subcategories.index');
    Route::get('create', 'SubCategoriesController@create')->name('subcategories.create');
    Route::post('store', 'SubCategoriesController@store')->name('subcategories.store');
    Route::get('edit/{id}', 'SubCategoriesController@edit')->name('subcategories.edit');
    Route::put('update/{id}', 'SubCategoriesController@update');
    Route::delete('delete/{id}', 'SubCategoriesController@destroy');
});
