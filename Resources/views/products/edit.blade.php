<div class="card rounded-1">
    <div class="card-body">
        <div class="modal-header">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Edit Product</h2>
                </div>
            </div>
        </div>


        {{-- Message Error --}}
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {{-- Form Edit Product --}}
        @foreach ($product as $item)
        <form action="{{ url('products/update/' . $item->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="modal-body">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        <input type="text" name="name" value="{{ $item->name }}" class="form-control"
                            placeholder="Name">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Detail:</strong>
                        <textarea class="form-control" style="height:150px" name="detail"
                            placeholder="Detail">{{ $item->detail }}</textarea>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-success">Update</button>
                    <a class="btn btn-default" href="{{route('products')}}">Back</a>
                </div>
            </div>
            @endforeach


        </form>
    </div>
</div>
