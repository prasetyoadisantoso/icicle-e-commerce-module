<aside class="right-side">

    {{-- Title Page Name --}}
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <h1>
                    Products
                    <small>&nbsp; Manage your products</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('ecommerce')}}"><i class="fa fa-dashboard"></i> Ecommerce</a></li>
                    <li class="active">Products</li>
                </ol>
            </div>
        </div>
    </section>

    {{-- Message Success --}}
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    {{-- Message Error --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


    {{-- Section Button --}}
    <section class="content">
        {{-- Create Button --}}
        <div class="pull-left">
            {{-- Some user with roles can access --}}
            @can('product-create')

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Create Products
            </button>


            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        {{-- Calling Create Page --}}
                        @include('ecommerce::products.create')

                    </div>
                </div>
            </div>
            @endcan
        </div>
    </section>


    {{-- Section Table --}}
    <section class="content">
        {{-- Table Management of Product List--}}
        <div class="box">
            <div class="row box-header">
                <div class="col-md-8">
                    <h3 class="box-title">List Products</h3>
                </div><!-- /.box-header -->
                <div class="col-md-4">
                    <div class="pull-right" style="margin-top: -15px; margin-bottom: -15px;">
                        {{-- Pagination --}}
                        {!! $products->links() !!}
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive">
                <table id="example1" class="example1 table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 50px; text-align: center;">No</th>
                            <th>Image</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($products as $item)
                        <tr>
                            <td style="width: 50px; text-align: center;">{{ ++$i }}</td>
                            <td class="text-center" style="width: 200px">

                                {{-- Single Picture --}}
                                {{Html::image(asset('images/' . json_decode($item->image)[0]), '', array('style' => 'width: 80px;')) }}


                                {{-- All Picture : Dont Erase--}}
                                {{-- @foreach (json_decode($item->image, TRUE) as $picture)
                        {{Html::image(asset('images/' . $picture), '', array('style' => 'width: 80px;')) }}
                                @endforeach --}}

                            </td>
                            <td>{{ $item->product }}</td>
                            <td>Rp {{ $item->regular_price }}</td>
                            <td>
                        </tr>
                        {{-- <form action="{{ url('products/delete/' . $product->id) }}" method="POST">
                        <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
                        @can('product-edit')
                        <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}" data-toggle="modal"
                            data-target="#editModal">Edit</a>
                        @endcan


                        @csrf
                        @method('DELETE')

                        @can('product-delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        @endcan

                        </form> --}}

                        <!-- Modal -->
                        <div class="modal rounded-2" id="editModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true"
                            style="background-color: white; margin-top:10%; margin-bottom: 10%; margin-left:10%; margin-right:10%; border-style: hidden;">
                        </div>
                        </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </section>


</aside>
