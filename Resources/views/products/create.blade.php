@can('product-create')
<div class="modal-header">
    <h3>Add New Product</h3>
</div>


{{-- Form Create Products --}}
<form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        @csrf
        <div class="row">

            {{-- Name of Product --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Product's Name:</strong>
                    <input type="text" name="product" class="form-control" placeholder="Product...">
                </div>
            </div>

            {{-- Brand of Product --}}
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Brand:</strong>
                    <textarea class="form-control" name="detail" placeholder="Detail"></textarea>
                </div>
            </div> --}}

            {{-- Caption --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Caption:</strong>
                    <input type="text" name="caption" class="form-control" placeholder="Caption...">
                </div>
            </div>

            {{-- Regular Price --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Regular Price:</strong>
                    <input type="text" name="regular_price" class="form-control" placeholder="Price...">
                </div>
            </div>

            {{-- Sale Price --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Sale Price:</strong>
                    <input type="text" name="sale_price" class="form-control" placeholder="Sale...">
                </div>
            </div>

            {{-- Description --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <textarea class="form-control" name="description" placeholder="Detail"></textarea>
                </div>
            </div>

            {{-- Image --}}
            <div class="col-xs-12 col-sm-12 col-md-12">

                <strong>Add Product Image:</strong>
                <div class="input-group control-group increment">
                    <input type="file" name="image[]" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-success" type="button"><i
                                class="glyphicon glyphicon-plus"></i>Add</button>
                    </div>
                </div>

                <div class="clone hide">
                    <div class="control-group input-group" style="margin-top:10px">
                        <input type="file" name="filename[]" class="form-control">
                        <div class="input-group-btn">
                            <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i>
                                Remove</button>
                        </div>
                    </div>
                </div>

            </div>

            {{-- Submit --}}
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <hr>
                <button type="submit" class="btn btn-success"><i class="fa fa-fw fa-save"></i>Submit</button>
                <a href="{{route('products.index')}}" type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-remove-circle"></i> Cancel</a>
            </div>
        </div>
</form>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

      $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){
          $(this).parents(".control-group").remove();
      });

    });

</script>

@endcan
