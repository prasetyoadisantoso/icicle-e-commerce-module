<aside class="right-side">
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">

                <div class="pull-left">
                    <h2>Products</h2>
                </div>

                <div class="pull-right">

                    {{-- Some user with roles can access --}}
                    @can('product-create')

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Create Products
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                @include('products::create')

                            </div>
                        </div>
                    </div>
                    @endcan


                </div>
            </div>
        </div>


        {{-- Message Success --}}
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif

        {{-- Message Error --}}
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif


        {{-- Table Management of Product List--}}
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Details</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($products as $product)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->detail }}</td>
                    <td>
                        <form action="{{ url('products/delete/' . $product->id) }}" method="POST">
                            {{-- <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a> --}}
                            @can('product-edit')
                            <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}"
                                data-toggle="modal" data-target="#editModal">Edit</a>
                            @endcan


                            @csrf
                            @method('DELETE')

                            @can('product-delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                            @endcan

                        </form>

                        <!-- Modal -->
                        <div class="modal rounded-2" id="editModal" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true"
                            style="background-color: white; margin-top:10%; margin-bottom: 10%; margin-left:10%; margin-right:10%; border-style: hidden;">
                        </div>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        {{-- Pagination --}}
        {!! $products->links() !!}
        <section>

</aside>
