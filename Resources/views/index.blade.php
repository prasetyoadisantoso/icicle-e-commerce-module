@extends('adminlte.layouts.master')

@section('content-products')

{{-- Get data from sidebar from Ecommerce Partials --}}
@include('ecommerce::partials.sidebar')

{{-- Main Page Ecommerce --}}
<aside class="right-side">

    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <h1>
                    Ecommerce
                    <small>&nbsp; Manage your shop</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('ecommerce')}}"><i class="fa fa-dashboard"></i> Ecommerce</a></li>
                    {{-- <li class="active">Products</li> --}}
                </ol>
            </div>
        </div>
    </section>

    <section class="content">
        {{-- Widget --}}
        <div class="row">
            <div class="col-lg-3 col-xs-6 col-md-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            150
                        </h3>
                        <p>
                            Products
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6 col-md-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            53<sup style="font-size: 20px">%</sup>
                        </h3>
                        <p>
                            Inventory Statisctic
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6 col-md-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>
                            Rp 500.000,00
                        </h3>
                        <p>
                            Revenue
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6 col-md-6">
                <!-- small box -->
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>
                            3 <sup style="font-size: 20px">Type</sup>
                        </h3>
                        <p>
                            Shippings
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios7-briefcase-outline"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div><!-- ./col -->
        </div>



        <!-- top row -->
        <div class="row">
            <div class="col-xs-12 connectedSortable">

            </div><!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
</aside>


@endsection
