<aside class="right-side">

    {{-- Title Page Name --}}
    <section class="content-header">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <h1>
                    Brands
                    <small>&nbsp; Manage your brands</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('ecommerce')}}"><i class="fa fa-dashboard"></i> Ecommerce</a></li>
                    <li class="active">Brands</li>
                </ol>
            </div>
        </div>
    </section>

    {{-- Message Success --}}
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    {{-- Message Error --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


    {{-- Section Button --}}
    <section class="content">
        {{-- Create Button --}}
        <div class="pull-left">
            {{-- Some user with roles can access --}}
            @can('product-create')

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Create Products
            </button>


            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        {{-- Calling Create Page --}}
                        @include('ecommerce::products.create')

                    </div>
                </div>
            </div>
            @endcan
        </div>
    </section>


    {{-- Section Table --}}
    <section class="content">
        {{-- Table Management of Product List--}}
        <div class="container-fluid mx-5">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th>Brand</th>
                    </tr>
                    @foreach ($brands as $item)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td class="text-center" style="width: 200px">
                            {{-- Single Picture --}}
                            {{Html::image(asset('images/' . json_decode($item->image)), '', array('style' => 'width: 80px;')) }}
                            {{-- All Picture : Dont Erase--}}
                            {{-- @foreach (json_decode($item->image, TRUE) as $picture)
                        {{Html::image(asset('images/' . $picture), '', array('style' => 'width: 80px;')) }}
                            @endforeach --}}
                        </td>
                        <td>{{ $item->brand }}</td>
                    </tr>
                    {{-- <form action="{{ url('products/delete/' . $product->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
                    @can('product-edit')
                    <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}" data-toggle="modal"
                        data-target="#editModal">Edit</a>
                    @endcan


                    @csrf
                    @method('DELETE')

                    @can('product-delete')
                    <button type="submit" class="btn btn-danger">Delete</button>
                    @endcan

                    </form> --}}

                    <!-- Modal -->
                    <div class="modal rounded-2" id="editModal" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true"
                        style="background-color: white; margin-top:10%; margin-bottom: 10%; margin-left:10%; margin-right:10%; border-style: hidden;">
                    </div>
                    </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        {{-- Pagination --}}
        {!! $brands->links() !!}
    </section>


</aside>
