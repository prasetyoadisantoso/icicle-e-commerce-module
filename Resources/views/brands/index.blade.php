@extends('adminlte.layouts.master')

@section('content-products')

    {{-- Get data from sidebar administrator --}}
    @include('ecommerce::partials.sidebar')

    {{-- Show product list --}}
    @include('ecommerce::brands.show')

@endsection
